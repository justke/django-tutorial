import datetime

from django.urls import reverse
from django.test import TestCase
from django.utils import timezone

from .models import Question


# Create your tests here.
def create_question(question_text, days):
    time = timezone.now() + datetime.timedelta(days=days)
    return Question.objects.create(question_text=question_text, pub_date=time)


class QuestionIndexViewTests(TestCase):

    def test_no_questions(self):
        response = self.client.get(reverse('polls:index'))
        self.assertEqual(response.status_code, 200)
        self.assertContains(response, 'No polls are available.')
        self.assertQuerysetEqual(response.context['latest_question_list'], [])

    def test_past_question(self):
        create_question(question_text='Past Question.', days=-30)
        response = self.client.get(reverse('polls:index'))
        self.assertQuerysetEqual(response.context['latest_question_list'], ['<Question: Past Question.>'])

    def test_future_question(self):
        create_question(question_text='Future Question.', days=30)
        response = self.client.get(reverse('polls:index'))
        self.assertContains(response, 'No polls are available.')
        self.assertQuerysetEqual(response.context['latest_question_list'], [])

    def test_one_future_question_with_one_past_question(self):
        create_question(question_text='Past Question.', days=-30)
        create_question(question_text='Future Question.', days=30)
        response = self.client.get(reverse('polls:index'))
        self.assertQuerysetEqual(response.context['latest_question_list'], ['<Question: Past Question.>'])

    def test_two_past_questions(self):
        create_question(question_text='Past Question One.', days=-30)
        create_question(question_text='Past Question Two.', days=-29)
        response = self.client.get(reverse('polls:index'))
        self.assertQuerysetEqual(
            response.context['latest_question_list'],
            ['<Question: Past Question Two.>', '<Question: Past Question One.>']
        )


class QuestionDetailViewTests(TestCase):

    def test_future_question(self):
        future_question = create_question(question_text='Future Question.', days=30)
        url = reverse('polls:detail', args=[future_question.id])
        response = self.client.get(url)
        self.assertEqual(response.status_code, 404)

    def test_past_question(self):
        past_question = create_question(question_text='Past Question.', days=-30)
        url = reverse('polls:detail', args=[past_question.id])
        response = self.client.get(url)
        self.assertContains(response, past_question.question_text)


class QuestionResultsViewTests(TestCase):

    def test_future_question(self):
        future_question = create_question(question_text='Future Question.', days=30)
        url = reverse('polls:results', args=[future_question.id])
        response = self.client.get(url)
        self.assertEqual(response.status_code, 404)

    def test_past_question(self):
        past_question = create_question(question_text='Past Question.', days=-30)
        url = reverse('polls:results', args=[past_question.id])
        response = self.client.get(url)
        self.assertContains(response, past_question.question_text)


class QuestionModelTests(TestCase):

    def test_was_published_recently_with_future_question(self):
        '''
        was_published_recently() returns False for questions whose pub_date is in the future.
        '''
        time = timezone.now() + datetime.timedelta(days=30)
        future_question = Question(pub_date=time)
        self.assertIs(future_question.was_published_recently(), False)

    def test_was_published_recently_with_question_older_than_one_day(self):
        '''
        was_published_recently() returns False for questions whose pub_date is older than one day.
        '''
        time = timezone.now() - datetime.timedelta(days=2)
        future_question = Question(pub_date=time)
        self.assertIs(future_question.was_published_recently(), False)

    def test_was_published_recently_with_question_in_the_past_day(self):
        '''
        was_published_recently() returns True for questions whose pub_date is in the past day.
        '''
        time = timezone.now() - datetime.timedelta(hours=12)
        future_question = Question(pub_date=time)
        self.assertIs(future_question.was_published_recently(), True)
